package com.example.beautyproject.services.map;

import com.example.beautyproject.entity.Master;
import com.example.beautyproject.services.MasterService;
import com.example.beautyproject.services.config.MapImplementation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class MasterMapService extends AbstractMapService<Master, Long> implements MasterService {

    private static final Map<Long, Master> resource = new HashMap<>();

    @Override
    public Map<Long, Master> getResource() {
        return resource;
    }

    @Override
    public Collection<Master> findBySpec(String spec) {
        return null;//todo impl
    }
}
