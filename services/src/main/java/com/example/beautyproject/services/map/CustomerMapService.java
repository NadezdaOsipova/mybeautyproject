package com.example.beautyproject.services.map;

import com.example.beautyproject.entity.Customer;
import com.example.beautyproject.services.CustomerService;
import com.example.beautyproject.services.config.MapImplementation;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@MapImplementation
public class CustomerMapService extends AbstractMapService<Customer, Long> implements CustomerService {

    private static final Map<Long, Customer> resource = new HashMap<>();

    @Override
    public Map<Long, Customer> getResource() {
        return resource;
    }

    @Override
    public Collection<Customer> findByName(String name) {
        return null;//todo impl
    }
}
