package com.example.beautyproject.services;

import com.example.beautyproject.entity.Master;

import java.util.Collection;

public interface MasterService extends CrudService<Master, Long> {

    Collection<Master> findBySpec(String spec);
}
