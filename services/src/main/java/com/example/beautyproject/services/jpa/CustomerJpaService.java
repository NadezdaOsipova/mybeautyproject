package com.example.beautyproject.services.jpa;

import com.example.beautyproject.entity.Customer;
import com.example.beautyproject.services.CustomerService;
import com.example.beautyproject.services.config.JpaImplementation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Transactional(readOnly = true)
@JpaImplementation
public class CustomerJpaService extends AbstractJpaServices<Customer, Long> implements CustomerService {

    @Override
    public JpaRepository<Customer, Long> getRepository() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<Customer> findByName(String name) {
        throw new UnsupportedOperationException();
    }
}
