package com.example.beautyproject.services;

import com.example.beautyproject.entity.Customer;

import java.util.Collection;

public interface CustomerService extends CrudService<Customer, Long> {

    Collection<Customer> findByName(String name);
}
