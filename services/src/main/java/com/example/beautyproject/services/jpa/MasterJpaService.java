package com.example.beautyproject.services.jpa;

import com.example.beautyproject.entity.Master;
import com.example.beautyproject.services.MasterService;
import com.example.beautyproject.services.config.JpaImplementation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Transactional(readOnly = true)
@JpaImplementation
public class MasterJpaService extends AbstractJpaServices<Master, Long> implements MasterService {

    @Override
    public JpaRepository<Master, Long> getRepository() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<Master> findBySpec(String spec) {
        throw new UnsupportedOperationException();
    }
}
