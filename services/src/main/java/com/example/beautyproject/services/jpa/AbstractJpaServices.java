package com.example.beautyproject.services.jpa;

import com.example.beautyproject.entity.BaseEntity;
import com.example.beautyproject.services.BaseJpaService;
import com.example.beautyproject.services.CrudService;

import java.util.Collection;
import java.util.NoSuchElementException;

public abstract class AbstractJpaServices<T extends BaseEntity<ID>, ID> implements CrudService<T, ID>, BaseJpaService<T, ID> {

    @Override
    public T findById(ID id) {
        return getRepository().findById(id)
                .orElseThrow(() -> new NoSuchElementException("not found"));
    }

    @Override
    public void save(T entity) {
        getRepository().save(entity);
    }

    @Override
    public Collection<T> findAll() {
        return getRepository().findAll();
    }

    @Override
    public void delete(ID id) {
        getRepository().deleteById(id);
    }
}

