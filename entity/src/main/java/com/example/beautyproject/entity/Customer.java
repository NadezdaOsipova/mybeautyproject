package com.example.beautyproject.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
@Entity
@Table(name = "CUSTOMER")
public class Customer extends Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "BIRTH_DATE")
    private LocalDate birthDate;

    @Embedded
    private ContactDetails contactDetails;

    @OneToMany(mappedBy = "customer")
    private List<Visit> visits;
}
