package com.example.beautyproject.entity;

public enum ReceiptStatus {

    PENDING, PAYED, CANCELLED
}
