package com.example.beautyproject.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Getter
@RequiredArgsConstructor
public enum Role {

    ADMIN("Administrator "), MASTER("Master "), DIRECTOR("Director");

    private static final Map<String, Role> MAP = Arrays.stream(Role.values())
            .collect(Collectors.toMap(Role::getValue, Function.identity()));

    private final String value;

    public static Role getByValue(String value) {
        if (Objects.isNull(value)) {
            throw new RuntimeException();//создать спец. exception
        }
        Role role = MAP.get(value);
        return Objects.isNull(role) ? ADMIN : role;
    }
}
