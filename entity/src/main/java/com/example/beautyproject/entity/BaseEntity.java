package com.example.beautyproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.OffsetDateTime;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
@MappedSuperclass
public abstract class BaseEntity<ID> {
    @CreationTimestamp
    @Column(name = "CREATE_AT", nullable = false, updatable = false)
    private OffsetDateTime createdAt;
    @CreatedBy
    @Column(name = "CREATE_BY", updatable = false)
    private String createdBy;
    @UpdateTimestamp
    @Column(name = "LAST_MODIFIED_AT", nullable = false)
    private OffsetDateTime lastModifiedAt;
    @LastModifiedBy
    @Column(name = "LAST_MODIFIED_BY")
    private String lastModifiedBy;

    public abstract ID getId();
}
