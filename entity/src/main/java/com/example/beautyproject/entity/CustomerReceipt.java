package com.example.beautyproject.entity;

import com.example.beautyproject.annotation.View;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Getter
@Entity
@View
@Table(name = "customer_receipt")
public class CustomerReceipt {

    @Id
    private Long id;
    @Column(name = "RECEIPT_ID")
    private Long receiptId;
    @Column(name = "TOTAL_AMOUNT")
    private BigDecimal totalAmount;
    @Column(name = "payer")
    private String payerFirstName;
    @Column(name = "PAYER_PHONE")
    private String payerPhone;

}