package com.example.beautyproject.entity;

import com.example.beautyproject.converter.RoleConverter;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "EMPLOYEE")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "LOGIN")
    private String login;
    @Column(name = "PASSWORD")
    private String password;

    @Convert(converter = RoleConverter.class)
    @Column(name = "ROLE")
    private Role role;

    @ManyToMany(cascade = {CascadeType.REFRESH, CascadeType.DETACH})
    @JoinTable(name = "employee_permission",
            joinColumns = @JoinColumn(name = "employee_id"),
            inverseJoinColumns = @JoinColumn(name = "perm_id"))
    private Set<Permission> permissions;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(name = "EMPLOYEE_FILIALS",
            joinColumns = @JoinColumn(name = "EMPLOYEES_ID"),
            inverseJoinColumns = @JoinColumn(name = "FILIALS_ID"))
    private Set<Filial> filials;

    @Embedded
    private ContactDetails contactDetails;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "city", column = @Column(name = "city_reg")),
            @AttributeOverride(name = "street", column = @Column(name = "street_reg")),
            @AttributeOverride(name = "houseNumber", column = @Column(name = "houseNumber_reg")),
            @AttributeOverride(name = "apartmentNumber", column = @Column(name = "apartmentNumber_reg")),
            @AttributeOverride(name = "phone", column = @Column(name = "phone_reg"))
    })
    private ContactDetails registration;// адрес регистрации

}


