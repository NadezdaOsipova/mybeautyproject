package com.example.beautyproject.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "RECEIPT")
public class Receipt {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "AMOUNT")
    private BigDecimal amount;//количество

    @OneToMany(mappedBy = "receipt", cascade = CascadeType.ALL)
    private List<BeautyCare> beautyCareList;

    @OneToOne(mappedBy = "receipt")
    private Visit visit;

    @OneToMany(mappedBy = "receipt", cascade = CascadeType.ALL)
    private List<Product> product;

    @Builder.Default
    @Enumerated(EnumType.STRING)
    @Column(name = "RECEIPT_STATUS")
    private ReceiptStatus receiptStatus = ReceiptStatus.PENDING;
}
