package com.example.beautyproject.entity;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "FILIAL")
public class Filial {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "FILIAL_NAME")
    private String filialName;
    @Column(name = "CITY")
    private String city;

    @ManyToMany(mappedBy = "filials")
    private Set<Employee> employees;
}
