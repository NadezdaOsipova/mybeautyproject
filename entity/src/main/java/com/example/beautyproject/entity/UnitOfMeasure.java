package com.example.beautyproject.entity;


import javax.persistence.*;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "UOM")
public class UnitOfMeasure {

    @Id
    @Enumerated(EnumType.STRING)
    @Column(name = "CODE")
    private UOM code;
    @Column(name = "DESCRIPTION")
    private String description;
}
