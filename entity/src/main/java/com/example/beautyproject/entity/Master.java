package com.example.beautyproject.entity;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Master extends Person {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private Long id;
    @Column(name = "SPECIFICATION")
    private String specification;

    @OneToMany(mappedBy = "master")
    private List<Visit> visits;
}
