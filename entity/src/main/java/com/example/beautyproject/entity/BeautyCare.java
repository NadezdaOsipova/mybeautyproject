package com.example.beautyproject.entity;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name = "BEAUTY_CARE")
public class BeautyCare { //услуга

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;
    @Column(name = "NAME")
    private String name;//название услуги
    @Column(name = "PRICE")
    private BigDecimal price;//стоимость услуги

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "RECEIPT_ID")
    private Receipt receipt;
}
