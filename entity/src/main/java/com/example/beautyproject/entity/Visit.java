package com.example.beautyproject.entity;

import javax.persistence.*;
import java.time.OffsetDateTime;
import java.util.UUID;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Table(name = "MASTER_VISIT")
@Entity
public class Visit {

    @Id
    @GeneratedValue
    @Column(name = "ID")
    private UUID id;
    @Column(name = "VISIT_TIME")
    private OffsetDateTime time; // время записи

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "VISIT_NOTES_ID")
    private Notes notes;

    @OneToOne
    @JoinColumn(name = "RECEIPT_ID")
    private Receipt receipt;

    @ManyToOne
    @JoinColumn(name = "CUSTOMER_ID")
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "MASTER_ID")
    private Master master;
}
