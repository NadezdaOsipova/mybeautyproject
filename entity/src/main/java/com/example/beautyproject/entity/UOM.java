package com.example.beautyproject.entity;

public enum UOM {

    SACHET, PACK, PIECE
}
