package com.example.beautyproject.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Getter
@Setter
@Embeddable
public class ProductPK implements Serializable {

    @Column(name = "CODE")
    private String code;
    @Column(name = "CODE_PART")
    private String codePart;
}
