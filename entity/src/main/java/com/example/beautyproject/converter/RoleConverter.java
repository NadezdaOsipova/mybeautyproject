package com.example.beautyproject.converter;

import com.example.beautyproject.entity.Role;
import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;

@Component
public class RoleConverter implements AttributeConverter<Role, String> {


    @Override
    public String convertToDatabaseColumn(Role role) {
        return role.getValue();
    }

    @Override
    public Role convertToEntityAttribute(String s) {
        return Role.getByValue(s);
    }
}
