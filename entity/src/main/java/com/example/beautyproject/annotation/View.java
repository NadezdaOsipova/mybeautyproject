package com.example.beautyproject.annotation;

import org.hibernate.annotations.Immutable;

@Immutable
public @interface View {
}
