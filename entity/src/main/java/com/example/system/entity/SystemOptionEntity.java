package com.example.system.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Entity
@Table(name = "SYSTEM_OPTION_ENTITY")
public class SystemOptionEntity {

    @Id
    @Column(name = "ID")
    private String id;
    @Column(name = "VALUE")
    private String value;

}
