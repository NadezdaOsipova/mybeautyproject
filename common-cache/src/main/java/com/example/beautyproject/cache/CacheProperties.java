package com.example.beautyproject.cache;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "cache.ttl")
public class CacheProperties {

    private int masters;
    private int customers;
}
