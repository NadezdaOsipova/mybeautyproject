package com.example.beautyproject.repository;

import com.example.beautyproject.entity.Master;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MasterRepository extends JpaRepository<Master, Long> {
}
