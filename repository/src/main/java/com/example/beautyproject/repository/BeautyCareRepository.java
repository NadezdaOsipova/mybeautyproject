package com.example.beautyproject.repository;

import com.example.beautyproject.entity.BeautyCare;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BeautyCareRepository extends JpaRepository<BeautyCare, Long> {
}
