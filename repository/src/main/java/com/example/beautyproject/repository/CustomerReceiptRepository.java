package com.example.beautyproject.repository;

import com.example.beautyproject.entity.CustomerReceipt;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CustomerReceiptRepository extends JpaRepository<CustomerReceipt, Long> {

   List<CustomerReceipt> findByPayerFirstNameAndPayerPhone(String firstName, String phone);
}
