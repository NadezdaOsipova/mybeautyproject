package com.example.beautyproject.repository;

import com.example.beautyproject.entity.Notes;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotesRepository extends JpaRepository<Notes, Long> {


}
