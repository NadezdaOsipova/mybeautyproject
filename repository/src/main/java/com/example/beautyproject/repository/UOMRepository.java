package com.example.beautyproject.repository;

import com.example.beautyproject.entity.UOM;
import com.example.beautyproject.entity.UnitOfMeasure;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UOMRepository extends JpaRepository<UnitOfMeasure, UOM> {
}
