package com.example.beautyproject.repository;

import com.example.beautyproject.entity.Product;
import com.example.beautyproject.entity.ProductPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, ProductPK> {
}
