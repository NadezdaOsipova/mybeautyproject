create table DATABASECHANGELOGLOCK
(
    ID          INT     not null,
    LOCKED      BOOLEAN not null,
    LOCKGRANTED TIMESTAMP,
    LOCKEDBY    VARCHAR(255),
    constraint PK_DATABASECHANGELOGLOCK
        primary key (ID)
);

create table DATABASECHANGELOG
(
    ID            VARCHAR(255) not null,
    AUTHOR        VARCHAR(255) not null,
    FILENAME      VARCHAR(255) not null,
    DATEEXECUTED  TIMESTAMP    not null,
    ORDEREXECUTED INT          not null,
    EXECTYPE      VARCHAR(10)  not null,
    MD5SUM        VARCHAR(35),
    DESCRIPTION   VARCHAR(255),
    COMMENTS      VARCHAR(255),
    TAG           VARCHAR(255),
    LIQUIBASE     VARCHAR(20),
    CONTEXTS      VARCHAR(255),
    LABELS        VARCHAR(255),
    DEPLOYMENT_ID VARCHAR(10)
);

create table BARBER
(
    ID                 BIGINT    not null
        primary key,
    CREATED_AT         TIMESTAMP not null,
    CREATED_BY         VARCHAR(255),
    LAST_MODIFIED_AT   TIMESTAMP not null,
    LAST_MODIFIED_BY   VARCHAR(255),
    FIRST_NAME         VARCHAR(255),
    LAST_NAME          VARCHAR(255),
    SPECIFICATION      VARCHAR(255),
    DESCRIPTION_BARBER VARCHAR(255)
);

create table CUSTOMER
(
    ID               BIGINT auto_increment
        primary key,
    CREATED_AT       TIMESTAMP not null,
    CREATED_BY       VARCHAR(255),
    LAST_MODIFIED_AT TIMESTAMP not null,
    LAST_MODIFIED_BY VARCHAR(255),
    FIRST_NAME       VARCHAR(255),
    LAST_NAME        VARCHAR(255),
    BIRTH_DATE       DATE,
    PHONE            VARCHAR(255)
);

create table EMPLOYEE
(
    ID                   BIGINT auto_increment
        primary key,
    APARTMENT_NUMBER     VARCHAR(255),
    CITY                 VARCHAR(255),
    HOUSE_NUMBER         VARCHAR(255),
    PHONE                VARCHAR(255),
    STREET               VARCHAR(255),
    LOGIN                VARCHAR(255),
    PASSWORD             VARCHAR(255),
    APARTMENT_NUMBER_REG VARCHAR(255),
    CITY_REG             VARCHAR(255),
    HOUSE_NUMBER_REG     VARCHAR(255),
    PHONE_REG            VARCHAR(255),
    STREET_REG           VARCHAR(255),
    ROLE                 VARCHAR(255)
);

create table FILIAL
(
    ID          BIGINT auto_increment
        primary key,
    CITY        VARCHAR(255),
    FILIAL_NAME VARCHAR(255)
);

create table EMPLOYEE_FILIALS
(
    EMPLOYEES_ID BIGINT not null,
    FILIALS_ID   BIGINT not null,
    primary key (EMPLOYEES_ID, FILIALS_ID),
    constraint FK2NA2AWNN1PVPU5EVVRQP9ORCL
        foreign key (FILIALS_ID) references FILIAL (ID),
    constraint FKB3B7IQ9KI92U4M2DFR0V5X1SB
        foreign key (EMPLOYEES_ID) references EMPLOYEE (ID)
);

create table MANICURIST
(
    ID                     BIGINT    not null
        primary key,
    CREATED_AT             TIMESTAMP not null,
    CREATED_BY             VARCHAR(255),
    LAST_MODIFIED_AT       TIMESTAMP not null,
    LAST_MODIFIED_BY       VARCHAR(255),
    FIRST_NAME             VARCHAR(255),
    LAST_NAME              VARCHAR(255),
    SPECIFICATION          VARCHAR(255),
    DESCRIPTION_MANICURIST VARCHAR(255)
);

create table NOTES
(
    ID          BIGINT auto_increment
        primary key,
    DESCRIPTION VARCHAR(255)
);

create table PERMISSION
(
    ID   BIGINT auto_increment
        primary key,
    NAME VARCHAR(255)
);

create table EMPLOYEE_PERMISSION
(
    EMPLOYEE_ID BIGINT not null,
    PERM_ID     BIGINT not null,
    primary key (EMPLOYEE_ID, PERM_ID),
    constraint FKENQ4UTXB6OVUKVI5QQEFYNQVS
        foreign key (PERM_ID) references PERMISSION (ID),
    constraint FKKJ257PMKLA5SJ49HDPXQ0GONQ
        foreign key (EMPLOYEE_ID) references EMPLOYEE (ID)
);

create table RECEIPT
(
    ID     BIGINT auto_increment
        primary key,
    AMOUNT DECIMAL(19, 2)
);

create table BEAUTY_CARE
(
    ID         BIGINT auto_increment
        primary key,
    NAME       VARCHAR(255),
    PRICE      DECIMAL(19, 2),
    RECEIPT_ID BIGINT,
    constraint FKG67H790DCDAQHTFM5KEWMYWOP
        foreign key (RECEIPT_ID) references RECEIPT (ID)
);

create table MASTER_VISIT
(
    ID             BINARY not null
        primary key,
    VISIT_TIME     TIMESTAMP,
    VISIT_NOTES_ID BIGINT,
    RECEIPT_ID     BIGINT,
    constraint FKFPIFKJ58YLP5C3DDYD9BS3C0I
        foreign key (RECEIPT_ID) references RECEIPT (ID),
    constraint FKJ7I71EF3SPHEBFTI7K58O7AGN
        foreign key (VISIT_NOTES_ID) references NOTES (ID)
);

create table UNIT_OF_MEASURE
(
    CODE        VARCHAR(255) not null
        primary key,
    DESCRIPTION VARCHAR(255)
);

create table PRODUCT
(
    CODE                 VARCHAR(255) not null,
    CODE_PART            VARCHAR(255) not null,
    NAME                 VARCHAR(255),
    PRICE                DECIMAL(19, 2),
    RECEIPT_ID           BIGINT,
    UNIT_OF_MEASURE_CODE VARCHAR(255),
    primary key (CODE, CODE_PART),
    constraint FKEOAXT4SLQ51CIS1E0V436GH2P
        foreign key (RECEIPT_ID) references RECEIPT (ID),
    constraint FKHXFNY2UH80LY25SV0U6Y13CQP
        foreign key (UNIT_OF_MEASURE_CODE) references UNIT_OF_MEASURE (CODE)
);

