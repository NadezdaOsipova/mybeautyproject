package com.example.beautyproject.api;

import com.example.beautyproject.entity.CustomerReceipt;
import com.example.beautyproject.repository.CustomerReceiptRepository;
import com.example.system.entity.SystemOptionEntity;
import com.example.system.repositopy.SystemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RequestMapping("/")
@RestController
public class TestController {

    private final CustomerReceiptRepository customerReceiptRepository;
    private final SystemRepository systemRepository;

    @GetMapping("view")
    public List<CustomerReceipt> getView() {
        return customerReceiptRepository.findAll();
    }

    @GetMapping("system")
    public List<SystemOptionEntity> getSystem() {
        return systemRepository.findAll();
    }
}
