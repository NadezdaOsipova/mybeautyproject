package com.example.beautyproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyBeautyProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(MyBeautyProjectApplication.class, args);
    }

}
