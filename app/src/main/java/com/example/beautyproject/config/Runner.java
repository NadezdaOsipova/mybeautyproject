package com.example.beautyproject.config;

import com.example.beautyproject.entity.*;
import com.example.beautyproject.repository.*;
import lombok.RequiredArgsConstructor;
import org.jasypt.encryption.StringEncryptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

@RequiredArgsConstructor
@Component("mainRun")
public class Runner implements CommandLineRunner {

    @Value("ENC(DzANBAh6o89q98Q0ngIBChZ9OaFsHQPU)")
    private String dbPwd;

    @Qualifier("customEncryptor")
    @Autowired
    StringEncryptor stringEncryptor;

    private final VisitRepository visitRepository;
    private final NotesRepository notesRepository;
    private final ProductRepository productRepository;
    private final BeautyCareRepository beautyCareRepository;
    private final ReceiptRepository receiptRepository;
    private final PermissionRepository permissionRepository;
    private final EmployeeRepository employeeRepository;
    private final UOMRepository uomRepository;
    private final CustomerRepository customerRepository;
    private final MasterRepository masterRepository;

    @Override
    public void run(String... args) throws Exception {
        // createCustomer();
        // createVisit();
        // createReceipt();

        //  initData();
        //  createProduct();
        //createEmployee();
        // createMaster();
        // encryption();
    }

    private void createMaster() {
        Barber barber = Barber.builder()
                .firstName("Olga")
                .lastName("Klimovich")
                .specification("master 1 category")
                .descriptionBarber("haircut, styling, coloring")
                .build();
        Manicurist manicurist = Manicurist.builder()
                .firstName("Nadezda")
                .lastName("Vaitovich")
                .specification("master 1 category")
                .descriptionManicurist("classic manicure, hardware, pedicure")
                .build();
        masterRepository.saveAll(Arrays.asList(barber, manicurist));
    }

    private void createCustomer() {
        Customer customer = Customer.builder()
                .firstName("Nadezda")
                .lastName("Osipova")
                .contactDetails(ContactDetails.builder()
                        .phone("+375-29-191-21-20")
                        .build())
                .build();
        customerRepository.save(customer);
    }

    private void createEmployee() {

        Permission perm = permissionRepository.findByName("update_employee")
                .orElseThrow(NoSuchElementException::new);

        Filial filial = Filial.builder()
                .filialName("branch on Pritytskogo")
                .city("Minsk")
                .build();

        Employee employee1 = Employee.builder()
                .login("employee1")
                .password("1234")
                .role(Role.ADMIN)
                .permissions(Collections.singleton(perm))
                .filials(Collections.singleton(filial))
                .build();
        Employee employee2 = Employee.builder()
                .login("employee2")
                .password("2222")
                .role(Role.MASTER)
                .permissions(Collections.singleton(perm))
                .filials(Collections.singleton(filial))
                .build();
        employeeRepository.saveAll(Arrays.asList(employee1, employee2));
    }

    private void initData() {
        Permission permission1 = Permission.builder()
                .name("update_employee")
                .build();
        Permission permission2 = Permission.builder()
                .name("delete_employee")
                .build();
        Permission permission3 = Permission.builder()
                .name("create_employee")
                .build();
        Permission permission4 = Permission.builder()
                .name("view_employee")
                .build();
        permissionRepository.saveAll(Arrays.asList(permission1, permission2, permission3, permission4));

        UnitOfMeasure unitOfMeasure1 = UnitOfMeasure.builder()
                .code(UOM.SACHET)
                .description("SACHET")
                .build();
        UnitOfMeasure unitOfMeasure2 = UnitOfMeasure.builder()
                .code(UOM.PACK)
                .description("PACK")
                .build();
        UnitOfMeasure unitOfMeasure3 = UnitOfMeasure.builder()
                .code(UOM.PIECE)
                .description("PIECE")
                .build();
        uomRepository.saveAll(Arrays.asList(unitOfMeasure1, unitOfMeasure2, unitOfMeasure3));
    }

    private void createReceipt() {

        List<Visit> visits = visitRepository.findAll();

        Receipt receipt1 = Receipt.builder()
                .amount(BigDecimal.TEN)
                .visit(visits.get(0))
                .build();

        BeautyCare beautyCare1 = BeautyCare.builder()
                .name("manicure")
                .price(BigDecimal.ONE)
                .receipt(receipt1)
                .build();
        BeautyCare beautyCare2 = BeautyCare.builder()
                .name("pedicure")
                .price(BigDecimal.ONE)
                .receipt(receipt1)
                .build();
        receipt1.setBeautyCareList(Arrays.asList(beautyCare1, beautyCare2));
        visits.get(0).setReceipt(receipt1);
        receiptRepository.save(receipt1);

        Receipt receipt2 = Receipt.builder()
                .amount(BigDecimal.TEN)
                .visit(visits.get(1))
                .build();
        visits.get(1).setReceipt(receipt2);
        receiptRepository.save(receipt2);
        visitRepository.saveAll(visits);
    }

    private void createProduct() {
        UnitOfMeasure uom1 = uomRepository.findById(UOM.PIECE).orElseThrow();
        UnitOfMeasure uom2 = uomRepository.findById(UOM.SACHET).orElseThrow();
        Product product1 = Product.builder()
                .name("Ollin Professional MEGAPOLIS")
                .price(BigDecimal.TEN)
                .productId(ProductPK.builder()
                        .code("001")
                        .codePart("Shampoo")
                        .build())
                .unitOfMeasure(uom1)
                .build();

        Product product2 = Product.builder()
                .name("Ollin Professional CARE ")
                .price(BigDecimal.TEN)
                .productId(ProductPK.builder()
                        .code("002")
                        .codePart("Shampoo")
                        .build())
                .unitOfMeasure(uom1)
                .build();
        Product product3 = Product.builder()
                .name("Ollin Professional MEGAPOLIS ")
                .price(BigDecimal.TEN)
                .productId(ProductPK.builder()
                        .code("001")
                        .codePart("Hair Mask")
                        .build())
                .unitOfMeasure(uom2)
                .build();

        productRepository.saveAll(Arrays.asList(product1, product2, product3));

    }

    private void createVisit() {
        Customer customer = customerRepository.findAll().get(0);

        Notes notes1 = Notes.builder().description("description1").build();
        Notes notes2 = Notes.builder().description("description2").build();

        Visit visit1 = Visit.builder().
                time(OffsetDateTime.now())
                .notes(notes1)
                .customer(customer)
                .build();
        Visit visit2 = Visit.builder()
                .time(OffsetDateTime.now())
                .notes(notes2)
                .customer(customer)
                .build();
        visitRepository.saveAll(Arrays.asList(visit1, visit2));
    }

    private void encryption() {
        //    String pwd = "dev_pwd";
        //    String encrypt = stringEncryptor.encrypt(pwd);
        //  System.out.println(encrypt);

//        String decrypt = stringEncryptor.decrypt(encrypt);
//        System.out.println(decrypt);

        System.out.println(dbPwd);
    }
}
